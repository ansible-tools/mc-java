import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import static java.lang.Math.abs;

public class McJava {
    public static void main(String[] args) {
        var julian = new Player(
                "Julian",
                new Position(1,1,1)
        );

        var currentPosition = julian.position.get();
        julian.move(Axis.X, currentPosition.get(Axis.X)+1)
                .move(Axis.Y, currentPosition.get(Axis.Y)-1);

        julian.sprint();
        julian.move(Axis.Z, 3);

        julian.eat(3);

        var marco = new Player("Marco");
        var playersOnline = new PlayersList();
        playersOnline.add(julian, marco);

        var harvey = new Player("Harvey");
        playersOnline.add(harvey);
    }
}

class Player {
    private Integer food = 18;
    String name;
    Boolean isSprinting = false;
    Position position;
    Player(String newName) {
        name = newName;
        position = new Position(0,0,0);

        System.out.printf("Created %s \n", this);
    }
    Player (String newName, Position newPosition) {
        name = newName;
        position = newPosition;

        System.out.printf("Created %s \n", this);
    }
    public void sprint() {
        isSprinting = true;
        System.out.printf("%s started sprinting...\n", this.name);
    }
    public void walk() {
        isSprinting = false;
        System.out.printf("%s started walking...\n", this.name);
    }
    void eat(Integer amount ) {
        if ( (food + amount) < 18) {
            food += amount;
        } else {
            food = 18;
        }
        System.out.printf("%s is eating %d food\n%s\n", name, amount, this);
    }
    void burnFood(Integer amount) {
        if ( (food - amount) > 0 ) {
            food -= amount;
            return;
        }
        food = 0;
    }
    Player move(Axis axis, Integer newValue) {
        Integer axisCurrentValue = position.get(axis);
        Integer distance = abs(axisCurrentValue - newValue);
        Integer foodBefore = food;

        if (isSprinting) {
            burnFood(distance);
        }
        burnFood(distance);
        Integer burnedFood = foodBefore - food;
        position.set(axis, newValue);

        System.out.printf(
                "moving %s from %s=%d to %s=%d burned %d food\n%s\n",
                name,
                axis, axisCurrentValue,
                axis, newValue,
                burnedFood,
                this
        );

        return this;
    }

    @Override
    public String toString() {
        return String.format("""
        Player %s
         - %s
         - food %d
         - %s
        """,
                name,
                position,
                food,
                (isSprinting)? "is sprinting" : "is walking"
        );
    }
}
enum Axis {
    X, Y, Z,
}

class Position {
    HashMap<Axis, Integer> position = new HashMap<>() {{
        put(Axis.X, 0);
        put(Axis.Y, 0);
        put(Axis.Z, 0);
    }};
    Position() {}
    Position(Integer x, Integer y, Integer z) {
        position.put(Axis.X, x);
        position.put(Axis.Y, x);
        position.put(Axis.Z, x);
    }

    public void set(Axis axis, Integer value) {
        position.put(axis, value);
    }

    @Override
    public String toString() {
        return String.format(
                "position X=%d Y=%d Z=%d",
                position.get(Axis.X),
                position.get(Axis.Y),
                position.get(Axis.Z)
        );
    }

    public HashMap<Axis, Integer> get() {
        return position;
    }
    Integer get(Axis axis) {
       return position.get(axis);
    }
}

class PlayersList {
    private final ArrayList<Player> playersList = new ArrayList<>();

    public ArrayList<Player> getPlayers() {
        return playersList;
    }

    public void add(Player... players) {
        Collections.addAll(playersList, players);
        System.out.println(this);
    }

    @Override
    public String toString() {
        StringBuilder playersListView = new StringBuilder("Players online:\n");

        for ( Player player : playersList ) {
            playersListView.append(" - ").append(player.name).append("\n");
        }
        return playersListView.toString();
    }
}